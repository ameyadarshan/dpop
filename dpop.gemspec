Gem::Specification.new do |s|
  s.name        = "dpop"
  s.version     = "0.0.1"
  s.summary     = "DPoP wrapper for Go"
  s.description = "Gem wrapping the code from glab's dpop package, to implement DPoP backend validation"
  s.authors     = ["Security"]
  s.email       = "security@gitlab.com"
  s.files       = ["lib/dpop.rb"]
  s.homepage    = "TODO"
  s.license     = "MIT"
end